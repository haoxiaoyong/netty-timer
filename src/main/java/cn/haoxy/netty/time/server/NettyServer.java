package cn.haoxy.netty.time.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:40
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class NettyServer {
    public static void main(String[] args) {
        //创建一个线程组:用来处理网络事件,(接收客户端连接)
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        //创建一个线程组:用来处理网络事件(处理通道IO操作)
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        //创建服务端启动助手来配置参数
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitHandler());
            ChannelFuture cf = bootstrap.bind(8765).sync();
            cf.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

}
