package cn.haoxy.netty.time.server;

import cn.haoxy.netty.time.common.protoc.BaseRequest;
import cn.haoxy.netty.time.common.protoc.BaseResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.WriteTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:41
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class NettyServerHandler extends SimpleChannelInboundHandler<BaseRequest.RequestInfo> {

    private final static Logger LOGGER = LoggerFactory.getLogger(NettyServerHandler.class);
   /* @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Request request = (Request) msg;
        System.out.println("Server 收到数据..." + request.getAppid() + ", " + request.getPhone() + ",  " + request.getValue());
        Response response = new Response();
        response.setId("appid: "+request.getAppid());
        response.setName("phone: " + request.getPhone());
        response.setResponseMessage("预警值: " + request.getValue());
        ctx.writeAndFlush(response);
    }*/

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, BaseRequest.RequestInfo requestInfo) throws Exception {
        LOGGER.info("Server 收到数据-> id={},appId={}", requestInfo.getId(), requestInfo.getAppid());
        BaseResponse.ResponseInfo resp = BaseResponse.ResponseInfo.newBuilder()
                .setId(requestInfo.getId())
                .setName(requestInfo.getAppid())
                .setResponseMessage("服务端回应消息")
                .build();
        ctx.writeAndFlush(resp);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause instanceof ReadTimeoutException || cause instanceof WriteTimeoutException) {
            LOGGER.info("超时了...... "+cause.toString());
        }
        ctx.close();//直接关闭channel
    }

}
