package cn.haoxy.netty.time.server;

import cn.haoxy.netty.time.common.protoc.BaseRequest;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.timeout.ReadTimeoutHandler;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:41
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class ChannelInitHandler extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline
                /*
                Netty 提供的编码器：底层使用是 java 序列化技术,效率低,不建议使用
                //添加一个解码器
                .addLast("encoder", new ObjectEncoder())
                //添加一个编码器
                .addLast("decoder", new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)))
                */
                // google Protobuf 编解码
                .addLast(new ProtobufDecoder(BaseRequest.RequestInfo.getDefaultInstance()))
                .addLast(new ProtobufEncoder())
                .addLast(new ProtobufVarint32FrameDecoder())
                .addLast(new ProtobufVarint32LengthFieldPrepender())
                //超时Handler (当服务端和客户端在指定的时间以上没有进行通信,则会关闭相应的通道,主要为减少服务端资源的占用)
                .addLast(new ReadTimeoutHandler(5))
                .addLast(new NettyServerHandler());

    }

}
