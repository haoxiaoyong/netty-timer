package cn.haoxy.netty.time.job;

import cn.haoxy.netty.time.client.NettyClient;
import cn.haoxy.netty.time.common.CacheCollection;
import cn.haoxy.netty.time.common.Request;
import cn.haoxy.netty.time.common.protoc.BaseRequest;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import io.netty.util.TimerTask;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:42
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
@RestController
@RequestMapping(value = "time")
public class HashedWhellTimerController {

    static Timer timer = new HashedWheelTimer(50L, TimeUnit.MILLISECONDS, 512);

    /**
     * 这个 main 方法相当于一个 Contraller,但客户点击提交的时候触发这个方法.携带appid,设置的极限值(value) , 手机号;
     *
     * @param request
     */
    @RequestMapping(value = "task")
    public void taskTimer(@RequestBody Request request) {
        if (CacheCollection.get(request.getAppid()) != null) {
            CacheCollection.remover(request.getAppid());
        }
        CacheCollection.put(request.getAppid(), request.getValue());
       /* if (CacheCollection.getTimeOut(request.getAppid()) != null) {
            Timeout timeOut = CacheCollection.getTimeOut(request.getAppid());
            timeOut.cancel();
            System.out.println( timeOut.isCancelled());
        }*/
        TimerTask task = new TimerTask() {
            @Override
            public void run(Timeout timeout) throws Exception {
                //在这里根据 appid 去查询是哪个客户
                System.out.println("appId: " + request.getAppid());
                for (int i = 0; i < 100; i++) {
                    Thread.sleep(100);
                    System.out.println(i);
                    if (i == CacheCollection.get(request.getAppid())) {
                        BaseRequest.RequestInfo message = BaseRequest.RequestInfo.newBuilder().setId(i).setAppid(request.getAppid()).build();
                        NettyClient.handler(message);
                    }
                }
                //任务执行完成后再把自己添加到任务solt上
                addTask(this, request);
            }
        };
        addTask(task, request);
    }

    public static void addTask(TimerTask task, Request request) {
        //根据时长把task任务放到响应的solt上
        Timeout timeout = timer.newTimeout(task, 5, TimeUnit.SECONDS);
        //CacheCollection.putTimeOut(request.getAppid(), timeout);
    }

}
