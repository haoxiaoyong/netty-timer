package cn.haoxy.netty.time;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:36
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
@SpringBootApplication
public class TimerTaskApplication {
    public static void main(String[] args) {
        SpringApplication.run(TimerTaskApplication.class, args);
    }

}
