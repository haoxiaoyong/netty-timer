package cn.haoxy.netty.time.common;

import java.io.Serializable;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:39
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class Request implements Serializable {

    private String appid;

    private String phone;

    private Integer value;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
