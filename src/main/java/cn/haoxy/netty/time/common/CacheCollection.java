package cn.haoxy.netty.time.common;

import io.netty.util.Timeout;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 11:09
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class CacheCollection {

    private static Map<String, Integer> ImageCacheMap;

    private static Map<String, Timeout> TimeCacheMap;

    static {
        ImageCacheMap = new HashMap<>();
    }

    static {
        TimeCacheMap = new HashMap<>();
    }

    /**
     * 建立appid和预警值的关联
     *
     * @param appid
     * @param value
     */
    public static void put(String appid, Integer value) {
        ImageCacheMap.put(appid, value);
    }

    /**
     * 根据appid 删除指定的value
     *
     * @param appid
     */
    public static void remover(String appid) {
        Iterator<Map.Entry<String, Integer>> iterator = ImageCacheMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            if (appid.equals(entry.getKey())) {
                iterator.remove();
            }
        }
    }

    /**
     * 根据appid获取预警值
     *
     * @param appid
     * @return
     */
    public static Integer get(String appid) {
        return ImageCacheMap.get(appid);
    }


    public static void putTimeOut(String appid, Timeout timeout) {
        TimeCacheMap.put(appid, timeout);
    }

    public static Timeout getTimeOut(String appid) {
        return TimeCacheMap.get(appid);
    }
}
