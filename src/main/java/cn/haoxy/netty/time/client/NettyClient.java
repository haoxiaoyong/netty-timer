package cn.haoxy.netty.time.client;
import cn.haoxy.netty.time.common.protoc.BaseRequest;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.util.concurrent.TimeUnit;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:37
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class NettyClient {
    /*private static class SingletonHolder {
        static final NettyClient instance = new NettyClient();
    }

    public static NettyClient getInstance() {
        return SingletonHolder.instance;
    }*/

    private static EventLoopGroup group;
    private static Bootstrap b;
    private ChannelFuture cf;


    public NettyClient() {
        group = new NioEventLoopGroup();
        b = new Bootstrap();
        b.group(group).channel(NioSocketChannel.class)
                .handler(new LoggingHandler(LogLevel.INFO))
                .handler(new ChannelinitClientHandler());

    }

    public void connect() {
        try {
            this.cf = b.connect("127.0.0.1", 8765).sync();
            System.out.println("远程服务器已连接,可以进行数据交换");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ChannelFuture getChannelFuture() {
        if (this.cf == null) {
            this.connect();
        }
        if (!this.cf.channel().isActive()) {
            this.connect();
        }
        return cf;
    }

    /*public void main(String[] args)  {*/
    public static void handler(BaseRequest.RequestInfo requestInfo) throws Exception {
        final NettyClient client = new NettyClient();
        ChannelFuture cf = client.getChannelFuture();
        cf.channel().writeAndFlush(requestInfo).sync();
        cf.channel().closeFuture().sync();
        group.shutdownGracefully();
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    System.out.println("进入子线程");
                    final NettyClient client = new NettyClient();
                    ChannelFuture cf = client.getChannelFuture();
                    System.out.println(cf.channel().isActive());
                    System.out.println(cf.channel().isOpen());
                    //再次发送数据
                    Request request = new Request();
                    request.setId("" + 4);
                    request.setName("pro" + 4);
                    request.setRequestMessage("数据信息" + 4);
                    cf.channel().writeAndFlush(request);
                    cf.channel().closeFuture().sync();
                    System.out.println("子线程结束");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();*/
        System.out.println("断开连接,主线程结束");
    }

    public static void main(String[] args) throws InterruptedException {
        final NettyClient client = new NettyClient();
        ChannelFuture cf = client.getChannelFuture();
        for (int i = 1; i <= 3; i++) {
            BaseRequest.RequestInfo message = BaseRequest.RequestInfo.newBuilder().setId(i).setAppid("pro"+i).build();
            cf.channel().writeAndFlush(message).sync();
            TimeUnit.SECONDS.sleep(4);
        }
        cf.channel().closeFuture().sync();
    }

}
