package cn.haoxy.netty.time.client;

import cn.haoxy.netty.time.common.protoc.BaseResponse;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.timeout.ReadTimeoutHandler;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:38
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class ChannelinitClientHandler extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline
                /*
                 Netty 提供的编码器：底层使用是 java 序列化技术,效率低,不建议使用
                //添加一个解码器
                .addLast("encoder", new ObjectEncoder())
                //添加一个编码器
                .addLast("decoder", new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)))
                */
                //编码器
                .addLast(new ProtobufDecoder(BaseResponse.ResponseInfo.getDefaultInstance()))
                //解码器
                .addLast(new ProtobufEncoder())
                .addLast(new ProtobufVarint32FrameDecoder())
                .addLast(new ProtobufVarint32LengthFieldPrepender())
                .addLast(new ReadTimeoutHandler(5))
                .addLast(new NettyClientHandler());
    }

}
