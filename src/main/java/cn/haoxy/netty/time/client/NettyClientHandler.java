package cn.haoxy.netty.time.client;

import cn.haoxy.netty.time.common.protoc.BaseResponse;
import cn.haoxy.netty.time.server.NettyServerHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.WriteTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by haoxiaoyong on 2019/3/30 下午 10:38
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class NettyClientHandler extends SimpleChannelInboundHandler<BaseResponse.ResponseInfo> {

    private final static Logger LOGGER = LoggerFactory.getLogger(NettyClientHandler.class);
    /*@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Response response = (Response) msg;
        System.out.println("Client 收到数据..." + response.getId() + ", " + response.getName() + ",  " + response.getResponseMessage());
    }*/

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, BaseResponse.ResponseInfo responseInfo) {
        /*log.info("add Backlog -> employeeId={}, backlogHead={}, scheduledDate={}", new Object[]{
                backlog.getEmployeeId(), backlog.getBacklogHead(), backlog.getScheduledDate()});*/

        LOGGER.info("Client 收到数据-> id={},name={},responseMessage={}", responseInfo.getId(), responseInfo.getName(), responseInfo.getResponseMessage());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause instanceof ReadTimeoutException || cause instanceof WriteTimeoutException) {
            LOGGER.info("超时了...... " + cause.toString());
        }
        ctx.close();//直接关闭channel
    }
}
